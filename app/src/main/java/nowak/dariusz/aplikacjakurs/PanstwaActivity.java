package nowak.dariusz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PanstwaActivity extends AppCompatActivity {

    @BindView(R.id.countryListView) ListView countryListView;
    ArrayAdapter<String> adapter;
    public static final String TAG = "Panstwa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panstwa);
        ButterKnife.bind(this);

        String[] country = {"Polska", "Włochy", "Argentyna", "Brazylia", "Francja", "Niemcy",
                "Hiszpania", "Belgia", "Holandia", "Portugalia", "Nowa Zelandia", "Kolumbia",
                "Egipt", "Maroko", "Rosja", "Chiny"};

        ArrayList<String> countryToList = new ArrayList<>();
        countryToList.addAll(Arrays.asList(country));
        adapter = new ArrayAdapter<>(this, R.layout.row, countryToList);
        countryListView.setAdapter(adapter);

        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String country = (String)parent.getItemAtPosition(position);
                Log.d(TAG, country);
            }
        });
    }
}
