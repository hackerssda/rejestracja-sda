package nowak.dariusz.aplikacjakurs;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccelerometrActivity extends AppCompatActivity implements SensorEventListener {

    @BindView(R.id.textViewX) TextView txtDlaX;
    @BindView(R.id.textViewY) TextView txtDlaY;
    @BindView(R.id.textViewZ) TextView txtDlaZ;

    private SensorManager sensorManagerAcc;
    private SensorManager sensorMangerTemp;
    private SensorManager sensorManagerShake;
    private Sensor accelerometr;
    private Sensor temp;
    private Sensor shake;
    private float x, y, z;
    private float temperature;
    private float shakeFloatX, shakeFloatY;
    private long lastUpdate = 0;

    final String TAG = "DarekApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometr);

        ButterKnife.bind(this);

        sensorManagerAcc = (SensorManager)getApplicationContext().getSystemService(SENSOR_SERVICE);
        sensorMangerTemp = (SensorManager) getApplicationContext().getSystemService(SENSOR_SERVICE);
        sensorManagerShake = (SensorManager) getApplicationContext().getSystemService(SENSOR_SERVICE);
        accelerometr = sensorManagerAcc.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        temp = sensorMangerTemp.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        shake = sensorManagerShake.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManagerAcc.registerListener(AccelerometrActivity.this, accelerometr, SensorManager.SENSOR_DELAY_NORMAL);
        sensorMangerTemp.registerListener(this, temp, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManagerShake.registerListener(this, shake, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManagerAcc.unregisterListener(AccelerometrActivity.this);
        sensorMangerTemp.unregisterListener(this);
        sensorManagerShake.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        if(mySensor.getType()==Sensor.TYPE_ACCELEROMETER){
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
            txtDlaX.setText(Float.toString(x));
            txtDlaY.setText(Float.toString(y));
            txtDlaZ.setText(Float.toString(z));
        }

        Sensor mySecondSensor = event.sensor;
        if(mySecondSensor.getType()==Sensor.TYPE_AMBIENT_TEMPERATURE){
            temperature = event.values[0];

            if (temperature>25){
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                linearLayout.setBackgroundColor(Color.RED);

            }else {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
            linearLayout.setBackgroundColor(Color.BLUE);
        }
        }

        Sensor myThridSensor = event.sensor;
        if(myThridSensor.getType() == Sensor.TYPE_ACCELEROMETER){
            shakeFloatX = event.values[0];
            shakeFloatY = event.values[1];

            if(shakeFloatX >0){
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                linearLayout.setBackgroundColor(Color.RED);
            }else {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                linearLayout.setBackgroundColor(Color.BLUE);
            }
        }

        long curTime = System.currentTimeMillis();

        if ((curTime - lastUpdate) > 100) {
            long diffTime = (curTime - lastUpdate);
            lastUpdate = curTime;
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
