package nowak.dariusz.aplikacjakurs;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ZasobyActivity extends AppCompatActivity {
    Button change;
    Button play;
    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zasoby);
        change = (Button) findViewById(R.id.change);

        play = (Button)this.findViewById(R.id.play);
        mp = MediaPlayer.create(this, R.raw.metal_gear_solid);
        play.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v) {
                if (mp.isPlaying()) {
                    mp.pause();
                }
                else {
                    mp.start();
                }

            }
        });


    }

}
