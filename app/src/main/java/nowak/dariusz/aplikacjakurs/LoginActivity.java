package nowak.dariusz.aplikacjakurs;


import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginActivity extends AppCompatActivity {
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hideKeyboard();
    }

    public void singIn(View view) {
        playBeep();
        final TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.inputLayout1);
        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.inputLayout2);
        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        if (!validateEmail(username)) {
            usernameWrapper.setError("Wprowadź poprawny adres email!");
        } else{
            usernameWrapper.setErrorEnabled(false);
        }
        if (!validatePassword(password)) {
            passwordWrapper.setError("Wprowadzone hasło jest za krótkie!");
            } else {
                passwordWrapper.setErrorEnabled(false);
            }
    }

    public boolean validatePassword(String password) {
        return password.length() > 9;
    }

    public boolean validateEmail(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void register(View view) {
        playBeep();
        Intent intent = new Intent(LoginActivity.this, RejestracjaActivity.class);
        startActivity(intent);
    }

    public void playBeep(){
        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        mediaPlayer.start();
    }
}

