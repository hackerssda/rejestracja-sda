package nowak.dariusz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nowak.dariusz.secretphoto.PhotoSecretReturner;
import uk.co.senab.photoview.PhotoViewAttacher;

public class CzwarteActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView kontrolkaZdjecia;
    PhotoViewAttacher zoomer;


    @OnClick(R.id.button)
    public void wczytajZdjecie(){
        ImageLoader imageLoader = ImageLoader.getInstance();
        PhotoSecretReturner zwrocRandomoweZdjecie = new PhotoSecretReturner();
        String foto = zwrocRandomoweZdjecie.zwrocZdjecieLista();
        imageLoader.displayImage(foto, kontrolkaZdjecia);
        zoomer = new PhotoViewAttacher(kontrolkaZdjecia);
        zoomer.update();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_czwarte);

        ButterKnife.bind(this);

        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.
                Builder(getApplicationContext()).
                build();
        ImageLoader.getInstance().init(configuration);



    }
}
