package nowak.dariusz.aplikacjakurs;

import android.content.DialogInterface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;

public class RejestracjaActivity extends AppCompatActivity {
    AutoCompleteTextView autoCompleteTextView;
    CheckBox acceptanceCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejestracja);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.countryText);
        acceptanceCheckBox = (CheckBox) findViewById(R.id.acceptanceCheck);
        final EditText telephoneNumber = (EditText) findViewById(R.id.telefonText);
        final EditText zipCode = (EditText) findViewById(R.id.zipCodeText);

        zipCode.setFilters(new InputFilter[] { new InputFilter.LengthFilter(6) });
        telephoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(9) });

        countries();

        zipCode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    zipCode.setText("");
                }
                return false;
            }
        });

        zipCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 2 ){
                    String convert = s + "-";
                    zipCode.setText(convert);
                    zipCode.setSelection(3);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
    }

    public void countries(){
        String[] country = {"Polska", "Włochy", "Argentyna", "Brazylia", "Francja", "Niemcy",
                "Hiszpania", "Belgia", "Holandia", "Portugalia", "Nowa Zelandia", "Kolumbia",
                "Egipt", "Maroko", "Rosja", "Chiny"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, country );
        autoCompleteTextView.setAdapter(adapter);
    }

    public void registerValidation(View view) {
        final TextInputLayout nameWrapper = (TextInputLayout) findViewById(R.id.LayoutName);
        final TextInputLayout surnameWrapper = (TextInputLayout) findViewById(R.id.LayoutSurname);
        final TextInputLayout zipcodeWrapper = (TextInputLayout) findViewById(R.id.LayoutTelephone);
        final TextInputLayout phoneWrapper = (TextInputLayout) findViewById(R.id.LayoutZipCode);
        final TextInputLayout loginWrapper = (TextInputLayout) findViewById(R.id.LayoutLogin);
        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.LayoutPassword);
        //final CheckBox acceptanceWrapper = (CheckBox) findViewById(R.id.acceptanceCheck);
        String login = loginWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        String name = nameWrapper.getEditText().getText().toString();
        String surname = surnameWrapper.getEditText().getText().toString();
        String zipCode = zipcodeWrapper.getEditText().getText().toString();
        String phone = phoneWrapper.getEditText().getText().toString();


        if (login.length() == 0) {
            loginWrapper.setError("Należy wypełnić to pole");
        } else{
            passwordWrapper.setErrorEnabled(false);
        }

        if (password.length() == 0) {
            loginWrapper.setError("Należy wypełnić to pole");
        } else{
            nameWrapper.setErrorEnabled(false);
        }


        if (name.length() == 0) {
            nameWrapper.setError("Należy wypełnić to pole");
        } else{
            nameWrapper.setErrorEnabled(false);
        }

        if (surname.length() == 0) {
            surnameWrapper.setError("Należy wypełnić to pole");
        } else{
            surnameWrapper.setErrorEnabled(false);
        }

        if (zipCode.length() == 0) {
            zipcodeWrapper.setError("Należy wypełnić to pole");
        } else{
            zipcodeWrapper.setErrorEnabled(false);
        }

        if (phone.length() == 0) {
            phoneWrapper.setError("Należy wypełnić to pole");
        }  else
        {
            phoneWrapper.setErrorEnabled(false);
        }

    if (!acceptanceCheckBox.isChecked()) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RejestracjaActivity.this);
        alertDialogBuilder.setTitle("Wymagana akceptacja regulaminu")
                .setMessage("Aby zakończyć rejestrację musisz zaakceptować regulamin")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
    }
}
