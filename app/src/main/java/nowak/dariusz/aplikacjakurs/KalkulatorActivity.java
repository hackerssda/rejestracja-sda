package nowak.dariusz.aplikacjakurs;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KalkulatorActivity extends AppCompatActivity {
    @BindView(R.id.horizontalText) EditText horizontal;
    @BindView(R.id.verticalText) EditText vertical;
    @BindView(R.id.diagonalText) EditText diagonal;
    @BindView(R.id.ResultText) TextView result;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalkulator);
        ButterKnife.bind(this);
    }

    public void calculationButton(View view) {
        if(!(horizontal.getText().toString().equals("") || vertical.getText().toString().equals("") || diagonal.getText().toString().equals(""))) {
            double horizontalValue = Double.parseDouble(horizontal.getText().toString());
            double verticalValue = Double.parseDouble(vertical.getText().toString());
            double diagonalValue = Double.parseDouble(diagonal.getText().toString());
            double resultValue = Math.sqrt((horizontalValue * horizontalValue) + (verticalValue * verticalValue)) / diagonalValue;
            mp = MediaPlayer.create(this, R.raw.beep);
            mp.start();
            result.setText(String.valueOf(String.format("%.2f", resultValue)) + " DPI");
        }

    }
}
